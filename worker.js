const { connect } = require("./utils");
const csvParser = require('./csvParser');


startWorker();

async function startWorker() {
    const workerId = process.pid;
    console.log(`Worker - ${workerId} started`);
    await connect();

    process.on("message", (packet) => {
        let data = packet.data
        console.log(`Worker - ${workerId} processing file:`, data.file);

        const start = process.hrtime(); // Enregistrer le temps de début du traitement

        csvParser(data.file).then(() => {
            const end = process.hrtime(start); // Enregistrer le temps de fin du traitement
            const durationMs = (end[0] * 1e9 + end[1]) / 1e6; // Convertir la durée en millisecondes
            const durationSec = Math.floor(durationMs / 1000); // Convertir la durée en secondes et arrondir vers le bas

            process.send({
                type: 'process:msg',
                data: {
                    state: 'idle',
                    file: data.file,
                    duration: durationSec
                }
            })
        })
    })

    process.send({
        type: 'process:msg',
        data: {
            state: 'idle'
        }
    })
}