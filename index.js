const fs = require("fs");
const path = require('path');
const pm2 = require('pm2');
const { connect, disconnect, dbLogs } = require("./utils");

const filesDirectory = '../SplittedCsv';
const files = fs.readdirSync(filesDirectory).filter(file => path.extname(file) === ".csv");

dbLogs();
start();

async function start() {
    await connect();

    const indexMax = files.length;
    let i = 0;
    let deletedCount = 0; // Compteur pour les processus supprimés
    let instances = Math.min(indexMax, 10); // Stocke le nombre d'instances
    const startTime = process.hrtime(); // Enregistrer le temps de début du processus

    pm2.start({
        script: 'worker.js',
        name: `worker`,
        exec_mode: 'cluster',
        instances: instances,
    }, (error) => {
        if (error) {
            console.error(error);
            pm2.disconnect();
            disconnect();
            process.exit(1);
        }
    });

    pm2.connect((err) => {
        if (err) {
            console.error(err);
            process.exit(2);
        }

        pm2.launchBus((workerError, bus) => {
            bus.on('process:msg', (packet) => {
                const workerId = packet.process.pm_id;
                console.log("Message received from Worker - ", workerId);
                const data = packet.data;
                if (data.state === 'idle') {
                    console.log("Working ...");
                    if (data.state === 'idle' && data.file) {
                        console.log(`File processed: ${data.file} - Duration: ${data.duration} seconds . Total files : ` + indexMax);
                    }
                    if (i < indexMax) {
                        pm2.sendDataToProcessId(workerId, {
                            id: workerId,
                            type: 'process:msg',
                            data: {
                                file: `${filesDirectory}/output-${i++}.csv`
                            },
                            topic: true
                        }, (messageError) => {
                            if (messageError) {
                                console.error(`Error when messaging ${workerId}`, messageError);
                            }
                        });
                    } else {
                        console.log("Deleting Worker - ", workerId);
                        pm2.delete(workerId, (err, proc) => {
                            if (!err) {
                                deletedCount++; // Incrémente le compteur lors de la suppression d'un processus
                                if (deletedCount === instances) {
                                    console.log("All workers have been deleted.");
                                    const endTime = process.hrtime(startTime); // Enregistrer le temps de fin du processus
                                    const totalTimeSeconds = endTime[0] + endTime[1] / 1e9; // Convertir en secondes
                                    const totalTimeMinutes = Math.floor(totalTimeSeconds / 60); // Convertir en minutes
                                    const remainingSeconds = Math.round(totalTimeSeconds % 60); // Calculer les secondes restantes
                                    console.log(indexMax + " Files have been processed.")
                                    console.log(`Total processing time: ${totalTimeMinutes} min ${remainingSeconds} s`);
                                    console.log("Job is done.");
                                    pm2.disconnect();
                                    disconnect();
                                }
                            }
                        });
                    }
                }
            });
        });
    });
}
