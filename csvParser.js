const fs = require('fs');
const readline = require('readline');
const Etablissement = require('./etablissementModel');
const { cleanData } = require('./utils');
const path = require('path');


const csvParser = async (filePath) => {
    const fileStream = fs.createReadStream(filePath);
    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });

    const isFirstFile = path.basename(filePath) === 'output-0.csv';
    let isFirstLine = true;
    let batch = []; // Stocke les données pour l'insertion en lot
    const batchSize = 1000; // Définir la taille du lot

    for await (const line of rl) {
        if (isFirstLine && isFirstFile) {
            isFirstLine = false;
            continue; // Ignore la première ligne (en-têtes)
        }

        const columns = line.split(','); // Sépare la ligne CSV en colonnes
        const etabData = {
            siren: cleanData(columns[0]),
            nic: cleanData(columns[1]),
            siret: cleanData(columns[2]),
            dateCreationEtablissement: cleanData(columns[4], "date"),
            dateDernierTraitementEtablissement: cleanData(columns[8], "date"),
            typeVoieEtablissement: cleanData(columns[16]),
            libelleVoieEtablissement: cleanData(columns[17]),
            codePostalEtablissement: cleanData(columns[18]),
            libelleCommuneEtablissement: cleanData(columns[19]),
            codeCommuneEtablissement: cleanData(columns[22]),
            dateDebut: cleanData(columns[43], "date"),
            etatAdministratifEtablissement: cleanData(columns[44])
        };

        batch.push(etabData);

        if (batch.length >= batchSize) {
            await Etablissement.insertMany(batch).catch(err => {
                console.error('Erreur lors de l\'insertion des lots:', err);
            });
            batch = []; // Vider le lot après l'insertion
        }
    }

    if (batch.length > 0) {
        await Etablissement.insertMany(batch).catch(err => {
            console.error('Erreur lors de l\'insertion des lots:', err);
        });
    }

    console.log(`Lecture et insertion terminées pour le fichier CSV : ${filePath}`);
};

module.exports = csvParser;
