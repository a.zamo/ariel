const csvSplitStream = require('csv-split-stream');
const fs = require('fs');
const path = require('path');

const inputCSVPath = path.join(__dirname, '../StockEtablissement_utf8.csv'); // Chemin relatif vers le fichier CSV d'entrée
const outputFolder = path.join(__dirname, '../SplittedCsv'); // Chemin relatif vers le dossier de sortie

// Créez le dossier de sortie s'il n'existe pas déjà
if (!fs.existsSync(outputFolder)) {
    fs.mkdirSync(outputFolder);
}

csvSplitStream.split(
    fs.createReadStream(inputCSVPath), // Utilisation du chemin relatif
    {
        lineLimit: 100000
    },
    (index) => fs.createWriteStream(path.join(outputFolder, `output-${index}.csv`)) // Utilisation du chemin relatif pour les fichiers de sortie dans le dossier de sortie
)
    .then(csvSplitResponse => {
        console.log('csvSplitStream succeeded.', csvSplitResponse);
    })
    .catch(csvSplitError => {
        console.log('csvSplitStream failed!', csvSplitError);
    });
